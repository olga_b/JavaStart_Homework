package Level1_Conditions;


public class IdToToyNameConverter {

    public String getToyName(int id) {
        try {
            if (id == 0) {
                return "Car";
            } else if (id == 1) {
                return "Lego";
            } else if (id == 2) {
                return "Doll";
            } else if (id == 3) {
                return "Puzzle";
            } else{
                throw new IllegalArgumentException();
            }
        } catch(IllegalArgumentException ex){
            return "Enter valid id";
        }
    }
    public int getToyId(String name){
        switch (name){
            case "Car": return 0;
            case "Lego": return 1;
            case "Doll": return 2;
            case "Puzzle": return 3;
            default: throw new IllegalArgumentException();
        }
    }
}
