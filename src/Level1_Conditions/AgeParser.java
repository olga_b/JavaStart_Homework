package Level1_Conditions;

/**
 * Created by olya on 06.01.18.
 */
public class AgeParser {
    private int age;

    public int parseAge(String ageString){

        try{
            age = Integer.valueOf(ageString);
            if (age < 1 || age > 120){
                throw new IllegalArgumentException(ageString);
            }
            else return age;

        }catch (IllegalArgumentException ex){
            System.out.println("Enter age in the range of 1...120");
            return -1;
        }

    }
}
