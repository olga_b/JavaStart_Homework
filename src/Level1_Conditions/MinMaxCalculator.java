package Level1_Conditions;

/**
 * Created by olya on 06.01.18.
 */
public class MinMaxCalculator {

    private int min;
    private int max;

    public void findMinMax(int a, int b) {

        if (a > b) {
            max = a;
            min = b;
        } else {
            min = a;
            max = b;
        }
        System.out.println("Max = "+max+" Min = "+min);
    }


}
