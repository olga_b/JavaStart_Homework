package Level2_Conditions_Loops;

import java.util.ArrayList;

public class PrimesPrinter {
    private ArrayList<Integer> arrayList = new ArrayList<>();
    private boolean isPrime;

    public void printPrimes(int from, int to) {

        checkArguments(from, to);
        for (int i = from; i < to + 1; i++) {
            if (i > 1) {
                arrayList.add(i);
            }
        }
        for (Integer digit : arrayList) {
            isPrime = true;
            for (int i = 2; i < digit; i++) {
                if (digit % i == 0) {
                    isPrime = false;
                    break;
                }

            }
            if (isPrime) {
                System.out.print(digit + " ");
            }

        }
    }

    public static void checkArguments(int from, int to){
        try {
            if (from > to) {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException ex) {
            System.out.println("Enter valid range");
        }
    }

}
