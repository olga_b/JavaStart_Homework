package Level2_Conditions_Loops;


public class RhombusPrinter {
    private int middle;
    private String[][] matrix;

    public void printRhombus(int size) {
        try {
            if (size <= 0 || size % 2 == 0) {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException ex) {
            System.out.println("Enter valid size");
            return;
        }
        matrix = new String[size][size];
        middle = Math.round(size / 2);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrix[i][j] = " ";
            }
        }
        for (int i = 0; i < middle + 1; i++) {
            matrix[i][middle - i] = "*";
            matrix[i][middle + i] = "*";
            matrix[size - i - 1][middle - i] = "*";
            matrix[size - i - 1][middle + i] = "*";

        }
        for (int i = 0; i < size; i++) {
            System.out.println();
            for (int j = 0; j < size; j++) {
                System.out.print(matrix[i][j]);
            }
        }

    }
}
