package Level2_Conditions_Loops;

import java.util.ArrayList;

public class DigitsSumCalculator {
    private int sum;
    private ArrayList<Integer> arrayList = new ArrayList<>();

    public int digitsSum(int from, int to) {

        PrimesPrinter.checkArguments(from, to);
        for (int i = from; i < to + 1; i++) {
            arrayList.add(i);
        }
        for (Integer number : arrayList) {
            if (number < 0) {
                number = Math.abs(number);
            }
            while (number != 0) {
                int lastDigit = number % 10;
                sum += lastDigit;
                number -= lastDigit;
                number /= 10;
            }
        }
        return sum;
    }
}
