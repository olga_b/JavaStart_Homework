package Level2_Conditions_Loops;

public class RadixPrinter {
    private int quotient;
    private int mod;
    private String result = "";

    public void printInRadix(int n, int radix) {

        if(radix == 10){
            result = String.valueOf(n);
            System.out.println(result);
            return;
        }
        try {
            if (radix < 2 || radix > 10 || n < 0)
                throw new IllegalArgumentException();
        } catch (IllegalArgumentException ex){
            System.out.println("Enter radix in the range of 2...10 and n > 0");
            return;
        }
        while (n != 0) {
            quotient = n / radix;
            mod = n % radix;
            result = mod + result;
            n = quotient;
        }
        System.out.println(result);
    }
}
