package Level2_Conditions_Loops;

public class DigitPrinter {

    private char sign;
    private String stringDigits = "";

    public void printDigits(int n){
        if(n < 0){
            n = Math.abs(n);
            sign = '-';
        }
        while(n != 0){
            int lastDigit = n % 10;
            stringDigits = lastDigit + " " + stringDigits;
            n-=lastDigit;
            n/=10;
        }
        System.out.println(String.valueOf(sign) + stringDigits);
    }
}
