package Level1_Loops;

public class RangePrinter {

    public void printRange(int first, int last) {

        if (first < last) {
            for (int i = first; i < last + 1; i++) {
                System.out.print(i);
            }
        } else {
            for (int i = first; i > last - 1; i--) {
                System.out.print(i);
            }
        }
        System.out.println();
    }

    public void printRange(int first, int last, int step) {
        if (first < last) {
            try {
                if (first + step < first) {
                    throw new IllegalArgumentException();
                }
                for (int i = first; i < last + 1; i += step) {
                    System.out.print(i);
                }
            } catch (IllegalArgumentException ex) {
                System.out.println("Enter valid step");
            }
        } else {
            try {
                if (first - step > first) {
                    throw new IllegalArgumentException();
                }
                for (int i = first; i > last - 1; i -= step) {
                    System.out.print(i);
                }
            } catch (IllegalArgumentException ex) {
                System.out.println("Enter valid step");
            }
        }
        System.out.println();

    }

    public void printEvensRange(int first, int last) {
        if (first > last) {
            return;
        } else {
            for (int i = first; i < last + 1; i++) {
                if (i % 2 == 0) {
                    System.out.print(i);
                }
            }
        }
        System.out.println();
    }

}
