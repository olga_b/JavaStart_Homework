package Level1_Loops;

public class FilledMatrixPrinter {

    public void printFilledMatrix(int rows, int cols, int filler){
        try{
            if(rows < 0 || cols < 0){
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException ex){
            System.out.println("Enter valid rows and cols");
        }
        for(int i=0; i< rows; i++){
            System.out.println();
            for(int j=0; j<cols; j++){
                System.out.print(filler+" ");
            }
        }
    }
}
