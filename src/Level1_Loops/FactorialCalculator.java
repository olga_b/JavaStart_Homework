package Level1_Loops;


public class FactorialCalculator {
    private long factorial;

    public long factorial(int n) {
        try {
            if (n < 1 || n > 120) {
                throw new IllegalArgumentException();
            }
            if (n == 1) {
                return 1;
            }
            factorial = n * factorial(n - 1);
        } catch (IllegalArgumentException ex) {
            System.out.println("Enter n in the range of 1...120");
            return -1;
        }
        return factorial;
    }
}
