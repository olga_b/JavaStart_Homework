import Level1_Arrays.ArrayFiller;
import Level1_Conditions.AgeParser;
import Level1_Conditions.IdToToyNameConverter;
import Level1_Conditions.IsAgeChecker;
import Level1_Conditions.MinMaxCalculator;
import Level1_Loops.FactorialCalculator;
import Level1_Loops.FilledMatrixPrinter;
import Level1_Loops.RangePrinter;
import Level2_Conditions_Loops.*;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        //Task 1. MinMaxCalculator
        System.out.println("\nMinMaxCalculator");
        MinMaxCalculator minMaxCalculator = new MinMaxCalculator();
        minMaxCalculator.findMinMax(0, 0);

        //Task 2. IsAgeChecker
        System.out.println("\nIsAgeChecker");
        IsAgeChecker isAgeChecker = new IsAgeChecker();
        System.out.println(isAgeChecker.isAge(1));

        //Task 3. AgeParser
        System.out.println("\nAgeParser");
        AgeParser ageParser = new AgeParser();
        System.out.println("Age is " + ageParser.parseAge("0"));

        //Task 4. IdToToyNameConverter
        System.out.println("\nIdToToyNameConverter");
        IdToToyNameConverter idToToyNameConverter = new IdToToyNameConverter();
        System.out.println(idToToyNameConverter.getToyName(-1));

        //Task 5. ToyNameToIdConverter
        System.out.println("\nToyNameToIdConverter");
        System.out.println(idToToyNameConverter.getToyId("Car"));

        //Level 1. Loops
        //Task 1. RangePrinter
        System.out.println("\nRangePrinter");
        RangePrinter rangePrinter = new RangePrinter();
        rangePrinter.printRange(1, 5);

        //Task 2. RangeWithStepPrinter
        System.out.println("\nRangeWithStepPrinter");
        rangePrinter.printRange(1, -6, 1);

        //Task 3. EvensRangePrinter
        System.out.println("\nEvensRangePrinter");
        rangePrinter.printEvensRange(2, 7);

        //Task 4. FactorialCalculator
        System.out.println("\nFactorialCalculator:");
        FactorialCalculator factorialCalculator = new FactorialCalculator();
        System.out.println(factorialCalculator.factorial(5));

        //Task 5. FilledMatrixPrinter
        System.out.println("\nFilledMatrixPrinter:");
        FilledMatrixPrinter filledMatrixPrinter = new FilledMatrixPrinter();
        filledMatrixPrinter.printFilledMatrix(2, 5, -1);

        //Level 2. Conditions + Loops
        //Task 1. DigitPrinter
        System.out.println("\nDigitPrinter:");
        DigitPrinter digitPrinter = new DigitPrinter();
        digitPrinter.printDigits(-123);

        //Task 2. RadixPrinter
        System.out.println("\nRadixPrinter:");
        RadixPrinter radixPrinter = new RadixPrinter();
        radixPrinter.printInRadix(-147292, 2);

        //Task 3. PrimesPrinter
        System.out.println("\nPrimesPrinter");
        PrimesPrinter primesPrinter = new PrimesPrinter();
        primesPrinter.printPrimes(101, 110);

        //Task 4. DigitsSumCalculator
        System.out.println("\n\nDigitsSumCalculator");
        DigitsSumCalculator digitsSumCalculator = new DigitsSumCalculator();
        System.out.println(digitsSumCalculator.digitsSum(147292, 147292));

        //Task 5. RhombusPrinter
        System.out.println("\nRhombusPrinter");
        RhombusPrinter rhombusPrinter = new RhombusPrinter();
        rhombusPrinter.printRhombus(7);

        //Level 1. Arrays
        //Task 1. ArrayFiller
        System.out.println("\nArrayFiller");
        ArrayFiller arrayFiller = new ArrayFiller();
        arrayFiller.fill(new int[5], 1);

        //Task 2. ArraySumCalculator
        System.out.println("\nArraySumCalculator");
        System.out.println(arrayFiller.sum(new int[]{2, 1, 3}));

        //Task 3. ArrayPositiveFinder
        System.out.println("\nArrayPositiveFinder");
        System.out.println(arrayFiller.findFirstPositiveElemIndex(new int[]{-3, 0, -1, 20, -2, 10}));
        System.out.println(arrayFiller.findLastPositiveElemIndex(new int[]{-3, 0, -1, 20, -2, 10}));
        System.out.println(arrayFiller.findFirstPositiveElemIndex(new int[]{-3, 0, -1}));

        //Task 4. ArrayPositivesCounter
        System.out.println("\nArrayPositiveCounter");
        System.out.println(arrayFiller.countPositives(new int[]{-3, 0, -1, 4, -2, 5}));
        System.out.println(arrayFiller.countPositives(new int[]{-3, 0, -1, -4, -2, -5}));

        //Task 5. SequenceArrayCreator
        System.out.println("\nSequenceArrayCreator");
        System.out.println(Arrays.toString(arrayFiller.createSequenceArray(5)));

        //Task 6. MatrixPrinter
        System.out.println("\nMatrixPrinter");
        arrayFiller.printMatrix(new int[][]{{1, 2, 3}, {3, -1, 0}, {4, 4, 4}});

        //Task 7. MatrixAverageCalculator
        System.out.println("\nMatrixAverageCalculator");
        System.out.println(arrayFiller.avg(new int[][]{
                {0, 3, -2},
                {2, 50, 3},
                {-1, 5, 2}
        }));

        //Task 8. MatrixMaxSumRowFinder
        System.out.println("\nMatrixMaxSumRowFinder");
        System.out.println(arrayFiller.maxSumRowIndex(new int[][]{
                {0, 3, -2},
                {2, 50, 3},
        }));
        int[][] empty = new int[5][5];
        System.out.println(arrayFiller.maxSumRowIndex(empty));
        System.out.println(arrayFiller.maxSumRowIndex(new int[][]{
                {},
                {},
                {}
        }));

        //Task 9. MatrixTransposer
        System.out.println("\nMatrixTransposer");
        int[][] target = arrayFiller.transpose(new int[][]{
                        {1, 2, 3, 4},
                        {5, 6, 7, 8}
                }
        );
        for (int i = 0; i < target.length; i++) {
            System.out.println();
            for (int j = 0; j < target[0].length; j++) {
                System.out.print(target[i][j]);
            }
        }

        //Task 10. IdentityMatrixChecker
        System.out.println("\nIdentityMatrixChecker");
        System.out.println(arrayFiller.isIdentity(new int[][]{
                {1, 0, 0},
                {0, 1, 0},
                {0, 0, 1}
        }));
        System.out.println(arrayFiller.isIdentity(new int[][]{
                {1, 0, 2},
                {0, 1, 0},
                {2, 0, 1}
        }));
        System.out.println(arrayFiller.isIdentity(new int[][]{
                {1, 0, 0},
                {0, 1, 0},
                {0, 0, 1},
                {0, 0, 0}
        }));

    }
}
