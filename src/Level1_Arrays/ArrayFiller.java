package Level1_Arrays;

import java.util.Arrays;

public class ArrayFiller {
    private int sum;
    private int counter;
    private int[] array;
    private int[] sumRow;
    private int[][] targetMatrix;

    public void fill(int[] array, int filler) {

        for (int i = 0; i < array.length; i++) {
            array[i] = filler;
        }
        System.out.println(Arrays.toString(array));

    }

    public int sum(int[] array) {

        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
    }

    public int findFirstPositiveElemIndex(int[] array) {

        for (int i = 0; i < array.length; i++) {
            if (array[i] > 0) {
                return i;
            }
        }
        return -1;
    }

    public int findLastPositiveElemIndex(int[] array) {

        for (int i = array.length - 1; i > 0; i--) {
            if (array[i] > 0) {
                return i;
            }
        }
        return -1;
    }

    public int countPositives(int[] array) {

        counter = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > 0) {
                counter++;
            }
        }
        return counter;
    }

    public int[] createSequenceArray(int size) {
        try {
            if (size < 0) {
                throw new IllegalArgumentException();
            }
            array = new int[size];
            for (int i = 0; i < size; i++) {
                array[i] = i;
            }
        } catch (IllegalArgumentException ex) {
            System.out.println("Enter valid size");
        }
        return array;
    }

    public void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            System.out.println();
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
        }
    }

    public int avg(int[][] matrix) {
        sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                sum += matrix[i][j];
            }
        }
        return sum / (matrix.length * matrix[0].length);
    }

    public int maxSumRowIndex(int[][] matrix) {

        int max = 0;
        int index = 0;
        try {
            if (matrix[0].length == 0) {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException ex) {
            System.out.println("Enter valid matrix");
            return -1;
        }
        sumRow = new int[matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                sumRow[i] += matrix[i][j];
            }
        }
        for (int i = 0; i < matrix.length; i++) {
            if (sumRow[i] > max) {
                max = sumRow[i];
                index = i;
            }
        }
        return index;
    }

    public int[][] transpose(int[][] sourceMatrix) {

        targetMatrix = new int[sourceMatrix[0].length][sourceMatrix.length];

        for (int j = 0; j < sourceMatrix[0].length; j++) {
            for (int i = 0; i < sourceMatrix.length; i++) {
                targetMatrix[j][i] = sourceMatrix[i][j];

            }
        }
        return targetMatrix;
    }

    public boolean isIdentity(int[][] matrix) {
        try {
            if (matrix.length != matrix[0].length) {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException ex) {
            System.out.println("Enter square matrix");
            return false;
        }
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if(i == j){
                    if(matrix[i][j] != 1){
                        return false;
                    }
                }
                else{
                    if(matrix[i][j] != 0){
                        return false;
                    }
                }
            }
        }
        return true;
    }

}
